const { _, Sequelize } = require('rappopo-sob').Helper

const schema = {
  icao: Sequelize.STRING(20),
  iata: Sequelize.STRING(20),
  iataAcc: Sequelize.STRING(20),
  hub: Sequelize.STRING(20),
  callSign: Sequelize.STRING(50),
  country: Sequelize.STRING(2),
  ageFleet: Sequelize.DOUBLE,
  founding: Sequelize.INTEGER,
  name: Sequelize.STRING(50),
  size: Sequelize.INTEGER,
  fleetNum: Sequelize.INTEGER,
  status: Sequelize.STRING(20),
  type: Sequelize.STRING(50)
}

const fields = _.concat(
  ['updatedAt'],
  _.keys(schema)
)

module.exports = ({ dbSql }) => {
  return {
    mixins: [dbSql],
    model: {
      define: schema,
      options: {
        createdAt: false,
        underscored: true
      }
    },
    settings: {
      singleService: true,
      fields
    },
    methods: {
    }
  }
}
