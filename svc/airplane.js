const { _, Sequelize } = require('rappopo-sob').Helper

const schema = {
  id: { type: Sequelize.STRING(20), allowNull: false, primaryKey: true }, // icao24 code
  regNum: Sequelize.STRING(20),
  iataType: Sequelize.STRING(20),
  iataAirline: Sequelize.STRING(20),
  icaoAirline: Sequelize.STRING(20),
  constNum: Sequelize.STRING(20),
  deliveryDt: Sequelize.DATEONLY,
  engType: Sequelize.STRING(20),
  engNum: Sequelize.INTEGER,
  firstFlight: Sequelize.DATEONLY,
  prodLineNum: Sequelize.STRING(20),
  modelCode: Sequelize.STRING(20),
  testRegNum: Sequelize.STRING(20),
  age: Sequelize.INTEGER,
  class: Sequelize.STRING(20),
  model: Sequelize.STRING(20),
  owner: Sequelize.STRING(100),
  series: Sequelize.STRING(20),
  status: Sequelize.STRING(20),
  prodLine: Sequelize.STRING(50),
  regDt: Sequelize.DATEONLY,
  rolloutDt: Sequelize.DATEONLY
}

const fields = _.concat(
  ['updatedAt'],
  _.keys(schema)
)

module.exports = ({ dbSql }) => {
  return {
    mixins: [dbSql],
    model: {
      define: schema,
      options: {
        createdAt: false,
        underscored: true
      }
    },
    settings: {
      singleService: true,
      fields
    },
    methods: {
    }
  }
}
