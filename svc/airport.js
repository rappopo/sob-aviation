const { _, Sequelize } = require('rappopo-sob').Helper

const schema = {
  iata: Sequelize.STRING(20),
  iataCity: Sequelize.STRING(20),
  icao: Sequelize.STRING(20),
  country: Sequelize.STRING(2),
  geonameId: Sequelize.INTEGER,
  lat: Sequelize.DOUBLE,
  lng: Sequelize.DOUBLE,
  name: Sequelize.STRING(50),
  phone: Sequelize.STRING(50),
  utc: Sequelize.STRING(10),
  tz: Sequelize.STRING(50)
}

const fields = _.concat(
  ['updatedAt'],
  _.keys(schema)
)

module.exports = ({ dbSql }) => {
  return {
    mixins: [dbSql],
    model: {
      define: schema,
      options: {
        createdAt: false,
        underscored: true
      }
    },
    settings: {
      singleService: true,
      fields
    },
    methods: {
    }
  }
}
